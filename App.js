import * as React from 'react';
import Navigator from './src/route';

const App = () => {
  return <Navigator />;
};

export default App;
