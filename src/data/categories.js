export default [
  {
    id: 1,
    name: 'Food & Drink',
    bg: 'red',
    sub_cat: [
      {
        id: 1,
        name: 'Milk tea',
      },
      {
        id: 2,
        name: 'Fast Food',
      },
      {
        id: 3,
        name: 'Coffee',
      },
      {
        id: 4,
        name: 'Sweet Cake',
      },
    ],
  },
  {
    id: 2,
    name: 'Clothing',
    bg: 'green',
    sub_cat: [
      {
        id: 1,
        name: 'T-shirt',
      },
      {
        id: '2',
        name: 'Bags',
      },
      {
        id: 3,
        name: 'Jeans',
      },
    ],
  },
  {
    id: 3,
    name: 'Gifts',
    bg: 'orange',
    sub_cat: [
      {
        id: 1,
        name: 'Tedy',
      },
      {
        id: 2,
        name: 'Handmade',
      },
      {
        id: 3,
        name: 'Ring',
      },
    ],
  },
  {
    id: 4,
    name: 'Mobile Phone',
    bg: 'pink',
    sub_cat: [
      {
        id: 1,
        name: 'Apple',
      },
      {
        id: 2,
        name: 'Samsung',
      },
      {
        id: 3,
        name: 'Oppo',
      },
    ],
  },
  {
    id: 5,
    name: 'Laptop',
    bg: 'navy',
    sub_cat: [
      {
        id: 1,
        name: 'HP',
      },
      {
        id: 2,
        name: 'Apple',
      },
      {
        id: 3,
        name: 'Dell',
      },
    ],
  },
];
