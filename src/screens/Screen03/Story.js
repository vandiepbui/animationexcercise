import React from 'react';
import {View, SafeAreaView, StyleSheet, Image} from 'react-native';

const Index = (props) => {
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.container}>
        <Image source={props.source} style={styles.image} />
      </View>
    </SafeAreaView>
  );
};

export default Index;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000',
  },
  image: {
    flex: 1,
    width: null,
    height: null,
  },
});
