import React from 'react';
import {View, StyleSheet, Animated, Dimensions, Platform} from 'react-native';

import Story from './Story';

const stories = [
  {
    id: 1,
    url: require('../../assets/images/1.jpg'),
  },
  {
    id: 2,
    url: require('../../assets/images/2.jpg'),
  },
  {
    id: 3,
    url: require('../../assets/images/3.jpg'),
  },
  {
    id: 4,
    url: require('../../assets/images/4.jpg'),
  },
  {
    id: 5,
    url: require('../../assets/images/5.jpg'),
  },
];

const {width} = Dimensions.get('window');
const perspective = width;

const Screen03 = (props) => {
  const x = new Animated.Value(0);

  const angle = Math.atan(perspective / (width / 2));
  const ratio = Platform.OS === 'ios' ? 2 : 1.2;

  const storyAnimation = (index) => {
    const offset = index * width;
    const inputRange = [offset - width, offset + width];

    const translateX = x.interpolate({
      inputRange,
      outputRange: [width / ratio, -width / ratio],
      extrapolate: 'clamp',
    });

    const rotateY = x.interpolate({
      inputRange,
      outputRange: [`${angle}rad`, `-${angle}rad`],
      extrapolate: 'clamp',
    });

    const translateX1 = x.interpolate({
      inputRange,
      outputRange: [width / 2, -width / 2],
      extrapolate: 'clamp',
    });

    const extra = width / ratio / Math.cos(angle / 2) - width / ratio;

    const translateX2 = x.interpolate({
      inputRange,
      outputRange: [extra, -extra],
      extrapolate: 'clamp',
    });

    return {
      ...StyleSheet.absoluteFillObject,
      transform: [
        {perspective},
        {translateX: translateX},
        {rotateY},
        {translateX: translateX1},
        {translateX: translateX2},
      ],
    };
  };

  const getBlackdropStyle = (index) => {
    const offset = index * width;
    const inputRange = [offset - width, offset, offset + width];
    const opacity = x.interpolate({
      inputRange,
      outputRange: [0.8, 0, 0.8],
      extrapolate: 'clamp',
    });
    return {
      backgroundColor: 'black',
      ...StyleSheet.absoluteFillObject,
      opacity,
    };
  };

  const feed = React.useMemo(() => {
    return stories.reverse().map((story, index) => {
      return (
        <Animated.View style={storyAnimation(index)} key={index}>
          <Story source={story.url} />
          <Animated.View style={getBlackdropStyle(index)} />
        </Animated.View>
      );
    });
  }, [stories]);

  return (
    <View style={styles.container}>
      {feed}
      <Animated.ScrollView
        style={StyleSheet.absoluteFillObject}
        bounces={false}
        showsHorizontalScrollIndicator={false}
        scrollEventThrottle={16}
        snapToInterval={width}
        contentContainerStyle={{width: width * stories.length}}
        onScroll={Animated.event(
          [
            {
              nativeEvent: {
                contentOffset: {x},
              },
            },
          ],
          {useNativeDriver: true},
        )}
        decelerationRate={0.5}
        horizontal
      />
    </View>
  );
};

export default Screen03;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000',
  },
});
