import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native';

import {Transition, Transitioning} from 'react-native-reanimated';

const transition = (
  <Transition.Together>
    <Transition.In type="fade" durationMs={200} />
    <Transition.Change />
    <Transition.Out type="fade" durationMs={200} />
  </Transition.Together>
);

import data from '../../data/categories';

const Screen01 = (props) => {
  const [categoryIndex, setCategoryIndex] = React.useState(null);

  const onPressCategory = (index) => {
    ref.current.animateNextTransition();
    setCategoryIndex(index === categoryIndex ? null : index);
  };

  const ref = React.useRef();
  return (
    <Transitioning.View
      style={styles.container}
      transition={transition}
      ref={ref}>
      <SafeAreaView />
      <Text style={styles.title}>CATEGORIES</Text>
      <View style={styles.body}>
        {data.map((item, index) => {
          return (
            <TouchableOpacity
              style={[styles.category, {backgroundColor: item.bg}]}
              key={index}
              onPress={() => onPressCategory(index)}>
              <Text style={styles.categoryName}>{item.name}</Text>
              {index === categoryIndex &&
                item.sub_cat.map((sub, index_sub) => {
                  return (
                    <TouchableOpacity
                      style={styles.subCategory}
                      key={index_sub}>
                      <Text style={styles.subCategoryName}>{sub.name}</Text>
                    </TouchableOpacity>
                  );
                })}
            </TouchableOpacity>
          );
        })}
      </View>
    </Transitioning.View>
  );
};

export default Screen01;

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
  },
  title: {
    fontSize: 28,
    color: '#424242',
    fontWeight: 'bold',
    marginBottom: 10,
    textAlign: 'center',
  },
  body: {
    flexGrow: 1,
  },
  category: {
    minHeight: 90,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 20,
  },
  categoryName: {
    fontSize: 20,
    color: '#fefefe',
    fontWeight: 'bold',
    textAlign: 'center',
    marginVertical: 10,
  },
  subCategoryName: {
    fontSize: 16,
    color: '#fefefe',
    fontWeight: 'bold',
    textAlign: 'center',
    paddingVertical: 10,
  },
});
