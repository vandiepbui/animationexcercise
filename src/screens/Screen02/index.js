import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  Image,
  StatusBar,
  Animated,
} from 'react-native';

const SPACING = 10;
const THUMBNAIL_SIZE = 80;
const ITEM_SIZE = THUMBNAIL_SIZE + SPACING * 3;

const Screen02 = (props) => {
  //Initial state
  const [photos, setPhotos] = React.useState([]);

  //useEffect
  React.useEffect(() => {
    getPhotos();
  }, []);

  const getPhotos = async () => {
    let res = await fetch(
      'https://jsonplaceholder.typicode.com/photos?_limit=50',
    );
    let resJson = await res.json();
    setPhotos(resJson);
    console.log(resJson);
  };

  //animation
  const scrollY = React.useRef(new Animated.Value(0)).current;

  const _renderItem = ({item, index}) => {
    const inputRange = [-1, 0, ITEM_SIZE * index, ITEM_SIZE * (index + 2)];
    const opacityInputRange = [
      -1,
      0,
      ITEM_SIZE * index,
      ITEM_SIZE * (index + 1),
    ];
    const scaleAnim = scrollY.interpolate({
      inputRange,
      outputRange: [1, 1, 1, 0],
    });

    const opacity = scrollY.interpolate({
      inputRange: opacityInputRange,
      outputRange: [1, 1, 1, 0],
    });
    return (
      <Animated.View
        style={[
          styles.itemWrapper,
          {transform: [{scale: scaleAnim}], opacity},
        ]}>
        <View style={styles.leftWrapper}>
          <Image
            source={{uri: item.thumbnailUrl}}
            style={styles.thumbnail}
            resizeMode="contain"
          />
        </View>
        <View style={styles.rightWrapper}>
          <Text style={styles.label} numberOfLines={2}>
            {item.title}
          </Text>
        </View>
      </Animated.View>
    );
  };
  return (
    <View style={styles.container}>
      <FlatList
        data={photos}
        renderItem={_renderItem}
        keyExtractor={(item, index) => `${index}`}
        extraData={photos}
        bounces={false}
        showsVerticalScrollIndicator={false}
        onScroll={Animated.event(
          [{nativeEvent: {contentOffset: {y: scrollY}}}],
          {useNativeDriver: false},
        )}
      />
    </View>
  );
};

export default Screen02;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 42,
    paddingHorizontal: 20,
    paddingVertical: 20,
    backgroundColor: '#ccc',
  },
  itemWrapper: {
    flexDirection: 'row',
    marginBottom: SPACING,
    padding: SPACING,
    backgroundColor: '#fff',
    borderRadius: 20,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.3,
    shadowRadius: 2,
  },
  leftWrapper: {
    marginRight: SPACING,
  },
  rightWrapper: {
    flex: 3,
    justifyContent: 'center',
  },
  thumbnail: {
    height: THUMBNAIL_SIZE,
    width: THUMBNAIL_SIZE,
    borderRadius: THUMBNAIL_SIZE,
  },
});
