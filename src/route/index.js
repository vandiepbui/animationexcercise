import * as React from 'react';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {NavigationContainer} from '@react-navigation/native';

import {Screen01, Screen02, Screen03} from '../screens';

const Drawer = createDrawerNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Drawer.Navigator initialRouteName="Screen01">
        <Drawer.Screen name="Screen01" component={Screen01} />
        <Drawer.Screen name="Screen02" component={Screen02} />
        <Drawer.Screen name="Screen03" component={Screen03} />
      </Drawer.Navigator>
    </NavigationContainer>
  );
}
